import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AngularFireAuthGuard, redirectLoggedInTo, redirectUnauthorizedTo } from '@angular/fire/auth-guard';


const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['']);
const redirectLoggedInToSendEmail = () => redirectLoggedInTo(['collection']);

const routes: Routes = [{
  path: '',
  loadChildren: () => import('./modules/authorization/authorization.module')
    .then((m) => m.AuthorizationModule),
  canActivate: [AngularFireAuthGuard],
  data: { authGuardPipe: redirectLoggedInToSendEmail}
}, {
  path: 'collection',
  loadChildren: () => import('./modules/collection/collection.module')
    .then((m) => m.CollectionModule),
  canActivate: [AngularFireAuthGuard],
  data: { authGuardPipe: redirectUnauthorizedToLogin }
}, {
  path: 'received',
  loadChildren: () => import('./modules/received/received.module')
    .then((m) => m.ReceivedModule),
  canActivate: [AngularFireAuthGuard],
  data: { authGuardPipe: redirectUnauthorizedToLogin }
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
