import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {SignupComponent} from './components/signup/signup.component';
import {OnboardingComponent} from './components/onboarding/onboarding.component';

const routes: Routes = [{
  path: '',
  component: OnboardingComponent
}, {
  path: 'signup',
  component: SignupComponent
}, {
  path: 'login',
  component: LoginComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthorizationRoutingModule { }
