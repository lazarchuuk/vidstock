import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {OnboardingComponent} from './components/onboarding/onboarding.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import {AuthorizationRoutingModule} from './authorization-routing.module';

import {SharedModule} from '../../shared/shared.module';

@NgModule({
  declarations: [OnboardingComponent, LoginComponent, SignupComponent],
    imports: [
        CommonModule,
        AuthorizationRoutingModule,
        SharedModule
    ]
})
export class AuthorizationModule {

}
