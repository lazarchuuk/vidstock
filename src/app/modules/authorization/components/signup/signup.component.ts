import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../../service/auth/auth.service';
import {passwordValidator} from '../../../../shared/validators/password.validator';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit, OnDestroy {
  public hide = true;
  public passwordControl = new FormControl('', [Validators.required]);
  public signupForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: this.passwordControl,
    repPassword: new FormControl('', [Validators.required, passwordValidator(this.passwordControl)])
  });

  constructor(public authService: AuthService) {
  }

  public getEmailErrorMessage(): string {
    return this.signupForm.get('email').hasError('required') ?
      'You must enter a value' :
      (this.signupForm.get('email').hasError('email') ? 'Not a valid email' : '');
  }

  public getPasswordErrorMessage(): string {
    return this.signupForm.get('repPassword').hasError('passwordMismatch') ?
      `Passwords aren't match` : '';
  }

  public signup(): void {
    this.authService.signup(this.signupForm.get('email').value, this.signupForm.get('password').value);
  }

  public ngOnInit(): void {
  }

  public ngOnDestroy(): void {
  }

}
