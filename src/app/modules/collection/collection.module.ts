import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CollectionComponent } from './components/collection.component';
import {CollectionRoutingModule} from './collection-routing.module';
import {LayoutModule} from '../layout/layout.module';
import {SharedModule} from '../../shared/shared.module';



@NgModule({
  declarations: [CollectionComponent],
  imports: [
    CommonModule,
    CollectionRoutingModule,
    LayoutModule,
    SharedModule
  ]
})
export class CollectionModule { }
