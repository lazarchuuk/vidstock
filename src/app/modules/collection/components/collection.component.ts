import {Component, OnDestroy, OnInit} from '@angular/core';
import {StorageService} from '../../../service/storage/storage.service';
import {map, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {ShareVideoService} from '../../../service/share-video/share-video.service';

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.css']
})
export class CollectionComponent implements OnInit, OnDestroy {
  public files: any[];
  private destroy$ = new Subject();

  constructor(private storageService: StorageService) {}

  public ngOnInit(): void {
    this.storageService.getFiles().pipe(takeUntil(this.destroy$)).subscribe(files => {
      this.files = files;
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
