import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './components/layout.component';
import {ProfileModule} from './modules/profile/profile.module';
import {HeaderModule} from './modules/header/header.module';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  declarations: [LayoutComponent],
  exports: [
    LayoutComponent
  ],
  imports: [
    CommonModule,
    ProfileModule,
    HeaderModule,
    SharedModule
  ]
})
export class LayoutModule { }
