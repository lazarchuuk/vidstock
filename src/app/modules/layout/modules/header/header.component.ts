import {Component, OnInit} from '@angular/core';
import * as firebase from 'firebase';
import {FormControl} from '@angular/forms';
import {AuthService} from '../../../../service/auth/auth.service';
import {UploadPopupComponent} from '../../../../shared/upload-popup/upload-popup.component';
import {MatDialog} from '@angular/material/dialog';
import {SharePopupComponent} from '../../../../shared/share-popup/share-popup/share-popup.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public user$ = this.auth.getUser();
  public mode = new FormControl('over');

  constructor(private auth: AuthService, public dialog: MatDialog) {
  }

  public ngOnInit(): void {
  }

  public logout(): void {
    return this.auth.logout();
  }

  public openDialog(): void {
    this.dialog.open(UploadPopupComponent);
  }
}
