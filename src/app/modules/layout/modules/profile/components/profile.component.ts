import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../../../service/auth/auth.service';
export * from '../../header/header.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  public user$ = this.authService.getUser();
  constructor(private authService: AuthService) { }

  public ngOnInit(): void {
  }

  public logout(): void {
    return this.authService.logout();
  }
}
