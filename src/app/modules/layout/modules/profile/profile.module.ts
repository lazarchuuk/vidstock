import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ProfileComponent} from './components/profile.component';
import {SharedModule} from '../../../../shared/shared.module';
import {CollectionRoutingModule} from '../../../collection/collection-routing.module';



@NgModule({
  declarations: [ProfileComponent],
  exports: [
    ProfileComponent
  ],
    imports: [
        CommonModule,
        SharedModule,
        CollectionRoutingModule
    ]
})
export class ProfileModule { }
