import {Component, OnDestroy, OnInit} from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {StorageService} from '../../../service/storage/storage.service';
import {ShareVideoService} from '../../../service/share-video/share-video.service';

@Component({
  selector: 'app-shared',
  templateUrl: './received.component.html',
  styleUrls: ['./received.component.css']
})
export class ReceivedComponent implements OnInit, OnDestroy {
  public receivedFiles: any[];
  private destroy$ = new Subject();

  constructor(private storageService: StorageService) {
  }

  public ngOnInit(): void {
    this.storageService.getReceivedFiles().pipe(takeUntil(this.destroy$)).subscribe(files => {
      this.receivedFiles = files;
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
