import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ReceivedComponent} from './components/received.component';

const routes: Routes = [{
  path: '',
  component: ReceivedComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceivedRoutingModule { }
