import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReceivedComponent } from './components/received.component';
import {ReceivedRoutingModule} from './received-routing.module';
import {SharedModule} from '../../shared/shared.module';
import {LayoutModule} from '../layout/layout.module';



@NgModule({
  declarations: [ReceivedComponent],
    imports: [
        CommonModule,
        ReceivedRoutingModule,
        SharedModule,
        LayoutModule
    ]
})
export class ReceivedModule { }
