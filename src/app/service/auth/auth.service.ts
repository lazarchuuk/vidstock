import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AngularFireAuth} from '@angular/fire/auth';
import {Router} from '@angular/router';
import * as firebase from 'firebase/app';
import {UserData} from './userData.class';
import {AngularFireDatabase} from '@angular/fire/database';
import {UserService} from '../user/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private angularFireAuth: AngularFireAuth,
              private angularFireDataBase: AngularFireDatabase,
              private router: Router,
              private userService: UserService) {
    this.user$ = angularFireAuth.authState;
  }


  private readonly user$: Observable<firebase.User>;

  public signup(email: string, password: string): void {
    this.angularFireAuth
      .createUserWithEmailAndPassword(email, password)
      .then((val) => {
        const userData = new UserData(val.user.uid, val.user.email);
        this.userService.saveUser(userData);
        return this.router.navigateByUrl('/collection');
      })
      .catch(err => {
        console.error('Something went wrong:', err.message);
      });
  }

  public login(email: string, password: string): void {
    this.angularFireAuth
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        return this.router.navigateByUrl('/collection');
      })
      .catch(err => {
        console.error(`Error: ${err}`);
      });
  }

  public logout(): void {
    this.angularFireAuth
      .signOut()
      .then(() => {
        return this.router.navigateByUrl('/');
      });
  }

  public getUser(): Observable<firebase.User> {
    return this.user$;
  }
}
