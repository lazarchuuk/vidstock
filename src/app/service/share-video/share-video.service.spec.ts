import { TestBed } from '@angular/core/testing';

import { ShareVideoService } from './share-video.service';

describe('ShareVideoService', () => {
  let service: ShareVideoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ShareVideoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
