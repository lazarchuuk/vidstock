import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {SharePopupComponent} from '../../shared/share-popup/share-popup/share-popup.component';
import {MatDialog} from '@angular/material/dialog';

@Injectable({
  providedIn: 'root'
})
export class ShareVideoService {
  public email: string;

  constructor(public dialog: MatDialog) {
  }

  public openDialog(): Observable<any> {
    const dialogRef = this.dialog.open(SharePopupComponent);
    return dialogRef.afterClosed();
  }
}
