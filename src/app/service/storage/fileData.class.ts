import {generateId} from '../../shared/utills/generate-id';

export class FileData {
  public key: string;
  public id: string;
  public userId: string;
  public name: string;
  public url: string;
  public file: File;
  public accessIds = [''];

  constructor(file: File, name: string) {
    this.file = file;
    this.name = name;

    this.id = generateId();
  }
}
