import {Injectable} from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {AngularFireStorage} from '@angular/fire/storage';
import {FileData} from './fileData.class';
import {from, Observable, ReplaySubject} from 'rxjs';
import {finalize, first, map, switchMap} from 'rxjs/operators';
import {AuthService} from '../auth/auth.service';
import {AngularFireAuth} from '@angular/fire/auth';
import {UserService} from '../user/user.service';

@Injectable({
  providedIn: 'root'
})

export class StorageService {

  constructor(private authService: AuthService,
              private angularFireDataBase: AngularFireDatabase,
              private angularFireAuth: AngularFireAuth,
              private angularFireStorage: AngularFireStorage,
              private userService: UserService) {
    this.angularFireAuth.onAuthStateChanged(user => {
      if (user) {
        this.userId$.next(user.uid);
      }
    });
  }

  private static readonly path = `uploads`;

  private userId$ = new ReplaySubject<string>(1);

  public uploadToStorage(fileData: FileData): Observable<number> {
    return this.userId$.pipe(
      first(),
      switchMap((id) => {
        const filePath = `${StorageService.path}/${fileData.id}`;
        const storage = this.angularFireStorage.ref(filePath);
        const task = this.angularFireStorage.upload(filePath, fileData.file);

        task.snapshotChanges().pipe(
          finalize(() => {
            storage.getDownloadURL().subscribe(downloadURL => {
              fileData.url = downloadURL;
              fileData.userId = id;
              this.saveFileData(fileData);
            });
          })
        ).subscribe();

        return task.percentageChanges();
      })
    );
  }

  private saveFileData(fileData: FileData): void {
    this.angularFireDataBase.list(StorageService.path).push(fileData);
  }

  public getFiles(): Observable<FileData[]> {
    return this.userId$.pipe(
      switchMap((id) =>
        this.angularFireDataBase.list<FileData>(StorageService.path).snapshotChanges().pipe(
          map(changes => changes.map(c => ({key: c.payload.key, ...c.payload.val()}))),
          map((files) => files.filter(item => item.userId === id)),
        )
      )
    );
  }

  public getReceivedFiles(): Observable<FileData[]> {
    return this.userId$.pipe(
      switchMap((id) =>
        this.angularFireDataBase.list<FileData>(StorageService.path).snapshotChanges().pipe(
          map(changes => changes.map(c => ({key: c.payload.key, ...c.payload.val()}))),
          map((files) => files.filter(item => item.accessIds.includes(id)))
        )
      )
    );
  }

  public shareFile(fileData: FileData, email: string): void {
    this.userService.getUserIdByEmail(email).subscribe(
      (accessId) => {
        if (accessId) {
          const newAccess = [...(fileData.accessIds || []), accessId];
          this.updateFileData(fileData, newAccess).subscribe(
            // success
          );
        } else {
          // return snackBarService.error // success
        }
      }
    );
  }

  private updateFileData(fileData: FileData, newAccess): Observable<void> {
    return from(this.angularFireDataBase.list<FileData>(StorageService.path)
      .update(fileData.key, {accessIds: newAccess}));
  }

  public deleteFile(fileData: FileData): void {
    this.deleteFromDatabase(fileData.key)
      .subscribe(() => this.deleteFromStorage(fileData.id));
  }

  private deleteFromDatabase(key: string): Observable<void> {
    return from(this.angularFireDataBase.list(StorageService.path).remove(key));
  }

  private deleteFromStorage(id: string): void {
    this.angularFireStorage.ref(StorageService.path).child(id).delete();
  }
}
