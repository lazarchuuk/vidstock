import {Injectable} from '@angular/core';
import {UserData} from '../auth/userData.class';
import {AngularFireDatabase} from '@angular/fire/database';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private static readonly path = `users`;

  constructor(private angularFireDataBase: AngularFireDatabase) {
  }

  public saveUser(userData: UserData): void {
    this.angularFireDataBase.list(UserService.path).push(userData);
  }

  public getUsers(): Observable<UserData[]> {
    return this.angularFireDataBase.list<UserData>(UserService.path).snapshotChanges().pipe(
      map(changes => changes.map(c => ({key: c.payload.key, ...c.payload.val()}))),
    );
  }

  public getUserIdByEmail(email): Observable<string> {
    return this.getUsers().pipe(
      map(users => users.find(user => user.email === email)),
      map(user => user ? user.id : null)
    );
  }

}
