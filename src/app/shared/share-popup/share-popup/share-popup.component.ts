import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-share-popup',
  templateUrl: './share-popup.component.html',
  styleUrls: ['./share-popup.component.css']
})
export class SharePopupComponent implements OnInit {
  public email: string;

  constructor(private matDialogRef: MatDialogRef<null>) { }

  public ngOnInit(): void {
  }

  public close(): void {
    this.matDialogRef.close(this.email);
  }

}
