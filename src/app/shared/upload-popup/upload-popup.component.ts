import {Component, OnInit} from '@angular/core';
import {FileData} from '../../service/storage/fileData.class';
import {StorageService} from '../../service/storage/storage.service';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-upload-popup',
  templateUrl: './upload-popup.component.html',
  styleUrls: ['./upload-popup.component.css']
})
export class UploadPopupComponent implements OnInit {
  public selectedFiles: FileList;
  public currentFile: FileData;
  public percentage: number;
  public name = '';
  public label = 'Choose file';

  constructor(private storageService: StorageService,
              private dialogRef: MatDialogRef<UploadPopupComponent>) {
  }

  public ngOnInit(): void {
  }

  public selectFile(event): void {
    this.selectedFiles = event.target.files;
    this.label = this.selectedFiles.item(0).name;
  }

  public upload(): void {
    const file = this.selectedFiles.item(0);
    const fileName = this.name;
    this.selectedFiles = undefined;

    this.currentFile = new FileData(file, fileName);
    this.storageService.uploadToStorage(this.currentFile).subscribe(
      percentage => {
        this.percentage = Math.round(percentage);
        if (this.percentage === 100) { this.dialogRef.close(); }
      },
      err => {
        console.log(err);
      }
    );
  }
}
