export const passwordValidator = (passwordControl) => {
  return (confirmPasswordControl) => {
    return passwordControl.value !== confirmPasswordControl.value ?
      {passwordMismatch: true} : null;
  };
};
