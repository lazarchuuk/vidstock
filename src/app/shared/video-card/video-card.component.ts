import {Component, Input, OnInit} from '@angular/core';
import {StorageService} from '../../service/storage/storage.service';
import {FileData} from '../../service/storage/fileData.class';
import {MatDialog} from '@angular/material/dialog';
import {ShareVideoService} from '../../service/share-video/share-video.service';

@Component({
  selector: 'app-video-card',
  templateUrl: './video-card.component.html',
  styleUrls: ['./video-card.component.css']
})
export class VideoCardComponent implements OnInit {
  @Input() public fileData: FileData;
  @Input() public isReadonly = false;
  public email: string;

  constructor(private storageService: StorageService,
              private dialog: MatDialog,
              private shareVideoService: ShareVideoService) {
  }

  public ngOnInit(): void {
  }

  public openDialog(): void {
    this.shareVideoService.openDialog().subscribe( email => this.share(email));
  }

  public share(email): void {
    this.storageService.shareFile(this.fileData, email);
  }

  public delete(): void {
    this.storageService.deleteFile(this.fileData);
  }
}
