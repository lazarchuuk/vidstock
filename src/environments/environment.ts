// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAoSosYNb17UDqB0fQuAm7dMb4e_9zD8pk',
    authDomain: 'my-vidstock.firebaseapp.com',
    databaseURL: 'https://my-vidstock.firebaseio.com',
    projectId: 'my-vidstock',
    storageBucket: 'my-vidstock.appspot.com',
    messagingSenderId: '490205155702',
    appId: '1:490205155702:web:4dcec5780bf688de8c2a57'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
